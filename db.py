from pymongo import MongoClient

client = MongoClient("mongodb+srv://test:test@cluster0-7s3jb.mongodb.net/ChatDB?retryWrites=true&w=majority")


chat_db = client.get_database("ChatDB")
users_collection = chat_db.get_collection("users")
rooms_collection = chat_db.get_collection("rooms")

def save_user(username, password):
    users_collection.insert_one({'_id': username, 'password': password})

def get_user(username):
    user_data = users_collection.find_one({'_id': username})
    return User(user_data['_id'], user_data['password']) if user_data else None

def save_rooms(room):
    rooms_collection.insert_one({'room': room})

def delete_rooms(room):
    rooms_collection.remove({'room': room})



